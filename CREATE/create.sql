-- Projekt z przedmiotu: Obiektowe Bazy Danych.
-- Autor: Wojciech Szymczak.
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE SEQUENCE user_id_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
/
CREATE SEQUENCE user_details_id_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
/
CREATE SEQUENCE event_id_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE; 
 /
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace TYPE USER_DETAILS_TYP AS OBJECT (
  first_name VARCHAR(255),    
  last_name VARCHAR(255),   
  birth_date DATE,
  MEMBER PROCEDURE display_user_personal_details ( SELF IN OUT NOCOPY user_details_typ )  
);
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace TYPE BODY USER_DETAILS_TYP AS
  MEMBER PROCEDURE display_user_personal_details ( SELF IN OUT NOCOPY user_details_typ ) AS
  BEGIN
    DBMS_OUTPUT.PUT_LINE(first_name || ' ' || last_name || ' ' || TO_CHAR(birth_date));
  END display_user_personal_details;
END;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace TYPE role_typ AS OBJECT (
   role_name  VARCHAR2(30)
);
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace TYPE role_tab IS TABLE OF role_typ;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE TYPE USER_TYP AS OBJECT ( 
  idno NUMBER,   
  uname VARCHAR(32),    
  email VARCHAR(255),   
  pass VARCHAR(32),
  join_date DATE,
  user_details USER_DETAILS_TYP,
  urole role_tab,
  
  MAP MEMBER FUNCTION get_user_id RETURN NUMBER, 
  MEMBER PROCEDURE display_user_data ( 
    SELF IN OUT NOCOPY USER_TYP
  ),
  MEMBER PROCEDURE create_user_details (
    SELF IN OUT NOCOPY USER_TYP, user_details_typ IN USER_DETAILS_TYP
  ),
  MEMBER PROCEDURE create_user_details (
    SELF IN OUT NOCOPY USER_TYP,
    first_name IN VARCHAR,
    last_name IN VARCHAR,
    birth_date IN DATE
  ),
  MEMBER PROCEDURE add_user_role (
    SELF IN OUT NOCOPY USER_TYP,
    role_name IN VARCHAR
  ),
  MEMBER PROCEDURE delete_user_role (
    SELF IN OUT NOCOPY USER_TYP,
    role_name IN VARCHAR
  ), 
  MEMBER PROCEDURE write_user_roles (
    SELF IN OUT NOCOPY USER_TYP
  )
)
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE user_obj_tab OF user_typ
NESTED TABLE urole STORE AS user_obj_tab_roles_nest_tab;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE TYPE BODY USER_TYP AS
  MAP MEMBER FUNCTION get_user_id RETURN NUMBER AS
  BEGIN
    RETURN idno;
  END get_user_id;

  MEMBER PROCEDURE display_user_data ( 
    SELF IN OUT NOCOPY USER_TYP
  ) AS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('User ID: ' || idno || ' name: ' || uname || ' email: ' || email || ' password: ' || pass);
  END display_user_data;

  MEMBER PROCEDURE create_user_details (
    SELF IN OUT NOCOPY USER_TYP, user_details_typ IN USER_DETAILS_TYP
  ) AS
  BEGIN
    SELF.user_details := user_details_typ;
  END create_user_details;

  MEMBER PROCEDURE create_user_details (
    SELF IN OUT NOCOPY USER_TYP,
    first_name IN VARCHAR,
    last_name IN VARCHAR,
    birth_date IN DATE
  ) AS
  BEGIN
    SELF.user_details := user_details_typ(first_name, last_name, birth_date);
  END create_user_details;
  
  MEMBER PROCEDURE add_user_role (
    SELF IN OUT NOCOPY USER_TYP,
    role_name IN VARCHAR    
  ) AS
  role_count NUMBER;
  e_role_present EXCEPTION;
  BEGIN
    IF urole IS NULL THEN
      -- role_tab in user_typ have to be initialized if it's null.
      UPDATE user_obj_tab u SET urole=role_tab(role_typ(add_user_role.role_name)) WHERE u.uname=add_user_role.SELF.uname;      
      DBMS_OUTPUT.PUT_LINE('User: ' || add_user_role.SELF.uname || ' has first role: ' || add_user_role.role_name || '.');
    ELSE
      -- we can't let user have duplicated roles, so we check it.
      SELECT COUNT(*) INTO role_count FROM user_obj_tab u, TABLE(u.urole) r WHERE u.uname=add_user_role.SELF.uname AND r.role_name=add_user_role.role_name;
      IF role_count > 0 THEN
        RAISE e_role_present;
      END IF;
      -- now we can insert new user role.
      INSERT INTO TABLE(SELECT urole FROM user_obj_tab u WHERE u.uname=add_user_role.SELF.uname) VALUES(role_typ(role_name));           
      DBMS_OUTPUT.PUT_LINE('User: ' || add_user_role.SELF.uname || ' has new role: ' || add_user_role.role_name || '.');      
      COMMIT;
    END IF;
    EXCEPTION
      WHEN e_role_present THEN
        DBMS_OUTPUT.PUT_LINE('User: ' || add_user_role.SELF.uname || ' has already role: ' || add_user_role.role_name || '. No changes were made.');      
  END add_user_role;

  MEMBER PROCEDURE delete_user_role (
    SELF IN OUT NOCOPY USER_TYP,
    role_name IN VARCHAR
  ) AS
    e_role_absent EXCEPTION;
    role_count NUMBER;
  BEGIN
    IF urole IS NULL THEN    
      DBMS_OUTPUT.PUT_LINE('User has no roles. No changes were made.');
    ELSE  
      SELECT COUNT(*) INTO role_count FROM user_obj_tab u, TABLE(u.urole) r WHERE u.uname=delete_user_role.SELF.uname AND r.role_name=delete_user_role.role_name;
      IF role_count < 1 THEN
        RAISE e_role_absent;
      END IF;    
      DELETE FROM TABLE(SELECT urole FROM user_obj_tab u WHERE u.uname=delete_user_role.SELF.uname) r WHERE r.role_name=delete_user_role.role_name;           
      DBMS_OUTPUT.PUT_LINE('User: ' || delete_user_role.SELF.uname || ' role: ' || delete_user_role.role_name || ' was deleted.');      
      COMMIT;
    END IF;
    EXCEPTION
      WHEN e_role_absent THEN                  
        DBMS_OUTPUT.PUT_LINE('User: ' || delete_user_role.SELF.uname || ' has no role: ' || delete_user_role.role_name || '. No changes were made.');
  END delete_user_role;

  MEMBER PROCEDURE write_user_roles (
    SELF IN OUT NOCOPY USER_TYP
  ) AS
  BEGIN
    IF urole IS NULL THEN
      DBMS_OUTPUT.PUT_LINE('User: ' || write_user_roles.SELF.uname || ' has no roles.');
    ELSE    
      DBMS_OUTPUT.PUT_LINE('User: ' || write_user_roles.SELF.uname || ' roles:');
      FOR i IN urole.FIRST .. urole.LAST
        LOOP
          DBMS_OUTPUT.PUT_LINE(i || ': ' || urole(i).ROLE_NAME);
        END LOOP;
    END IF;
  END write_user_roles;
END;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace PACKAGE USER_ACTIONS
  AS
  PROCEDURE register_user (
    email VARCHAR,
    uname VARCHAR,
    pass VARCHAR
  );
  PROCEDURE update_user_details (
    uname VARCHAR,
    first_name VARCHAR,
    last_name VARCHAR,
    birth_date DATE
  );  
  PROCEDURE add_friend (
    uname1 VARCHAR,
    uname2 VARCHAR
  );
  PROCEDURE delete_friend (
    uname1 VARCHAR,
    uname2 VARCHAR
  );
  PROCEDURE add_user_role (
    uname VARCHAR,
    role_name VARCHAR
  );
  PROCEDURE delete_user_role (
    uname VARCHAR,
    role_name VARCHAR
  );
  PROCEDURE write_user_data (
    uname VARCHAR
  );
  PROCEDURE write_user_roles (
    uname VARCHAR
  );
  PROCEDURE write_user_friends (
    uname VARCHAR
  );
  PROCEDURE write_user_participated_events (
    uname VARCHAR
  );
  PROCEDURE write_user_admin_events (
    uname VARCHAR
  );
END USER_ACTIONS;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE user_friends_tab (
user_typ_1 ref user_typ not null,
user_typ_2 ref user_typ not null
);
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE TYPE EVENT_TYP AS OBJECT (
  idno NUMBER,
  ename VARCHAR(64),
  beginning_date DATE,
  event_longitude NUMBER,
  event_latitude NUMBER,
  event_description VARCHAR(512),
  
  MEMBER PROCEDURE display_event_data ( 
    SELF IN OUT NOCOPY EVENT_TYP
  )
)
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE event_participants_tab (
event_typ_1 REF event_typ NOT NULL,
user_typ_1 REF user_typ NOT NULL
);
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE event_administrators_tab (
event_typ_1 REF event_typ NOT NULL,
user_typ_1 REF user_typ NOT NULL
);
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE event_obj_tab OF event_typ
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE
TYPE BODY EVENT_TYP AS

  MEMBER PROCEDURE display_event_data ( 
    SELF IN OUT NOCOPY EVENT_TYP
  ) AS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Event id: ' || idno || ' name: ' || ename || ' beginning date: ' || beginning_date ||
    ' longitude: ' || event_longitude || ' latitude: ' || event_latitude || ' description: ' || event_description);
  END display_event_data;
END;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace PACKAGE EVENT_ACTIONS AS

  PROCEDURE create_event (
  ename VARCHAR,
  beginning_date DATE,
  event_longitude NUMBER,
  event_latitude NUMBER,
  event_description VARCHAR
  );
  PROCEDURE add_event_administrator (
    event_name VARCHAR,
    user_name VARCHAR
  );  
  PROCEDURE delete_event_administrator (
    event_name VARCHAR,
    user_name VARCHAR
  );
  PROCEDURE add_event_participant (
    event_name VARCHAR,
    user_name VARCHAR
  );
  PROCEDURE delete_event_participant (
    event_name VARCHAR,
    user_name VARCHAR
  );
  PROCEDURE write_event_administrators (
    event_name VARCHAR
  );
  PROCEDURE write_event_participants (
    event_name VARCHAR
  );
  PROCEDURE write_event_details (
    event_name VARCHAR
  );
END EVENT_ACTIONS;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
create or replace PACKAGE BODY USER_ACTIONS AS
  PROCEDURE register_user (
    email VARCHAR,
    uname VARCHAR,
    pass VARCHAR
  ) IS
    e_username_taken EXCEPTION;
    e_email_taken EXCEPTION;
    user_count NUMBER;
    user_typ_obj user_typ;
  BEGIN     

    -- check if email is taken
    SELECT COUNT(VALUE(u)) INTO user_count FROM user_obj_tab u WHERE u.EMAIL = register_user.email;        
    IF user_count != 0 THEN
      RAISE e_email_taken;
    END IF;
    -- and now check if username is taken
    SELECT COUNT(VALUE(u)) INTO user_count FROM user_obj_tab u WHERE u.UNAME = register_user.uname;
    IF user_count != 0 THEN    
      RAISE e_username_taken;      
    END IF;
    
    -- assigning variables for user's object.
    user_typ_obj := user_typ(user_id_seq.NEXTVAL, uname, email, pass, SYSDATE, null, null);

    -- if done checking, create user
    INSERT INTO user_obj_tab VALUES (user_typ_obj);
    DBMS_OUTPUT.put_line('User successfully created!');
    
    EXCEPTION
      WHEN e_username_taken THEN        
        DBMS_OUTPUT.put_line('Username is already taken! Please choose another one.');
      WHEN e_email_taken THEN
        DBMS_OUTPUT.put_line('Email is already taken! Please choose another one.');
  END register_user;
  
  PROCEDURE update_user_details (
    uname VARCHAR,
    first_name VARCHAR,
    last_name VARCHAR,
    birth_date DATE
  ) IS
    e_user_not_found EXCEPTION;
    e_duplicate_users EXCEPTION;
    user_count NUMBER;
    user_typ_obj user_typ;
    user_details user_details_typ;
  BEGIN   
  
    -- check if user is present or duplicated
    SELECT COUNT(VALUE(u)) INTO user_count FROM user_obj_tab u WHERE u.uname = update_user_details.uname;        
    IF user_count != 1 THEN
      RAISE e_user_not_found;
    ELSIF user_count > 1 THEN
      RAISE e_duplicate_users;    
    END IF;
    
    -- loading user object.
    SELECT VALUE(u) INTO user_typ_obj FROM user_obj_tab u WHERE u.uname = update_user_details.uname;

    -- if done checking, create user details
    user_details := user_details_typ(update_user_details.first_name, update_user_details.last_name, update_user_details.birth_date);
    user_typ_obj.user_details := user_details;
    UPDATE user_obj_tab u SET u = user_typ_obj WHERE u.uname = update_user_details.uname;
    DBMS_OUTPUT.put_line('User details successfully updated!');
    
    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('Username not found! Operation terminated.');
      WHEN e_duplicate_users THEN
        DBMS_OUTPUT.put_line('Duplicated users! Operation terminated.');
  END update_user_details;
  
  PROCEDURE add_friend (
    uname1 VARCHAR,
    uname2 VARCHAR
  ) IS
    e_duplicate_users EXCEPTION;
    e_user_not_found EXCEPTION;
    e_user_already_friends EXCEPTION;
    user_count NUMBER;
  BEGIN   

    -- check if user 1 is present
    SELECT COUNT(VALUE(u)) INTO user_count FROM user_obj_tab u WHERE u.uname = add_friend.uname1;        
    IF user_count != 1 THEN
      RAISE e_user_not_found;
    ELSIF user_count > 1 THEN
      RAISE e_duplicate_users;    
    END IF;
    
    -- check if user 2 is present
    SELECT COUNT(VALUE(u)) INTO user_count FROM user_obj_tab u WHERE u.uname = add_friend.uname2;        
    IF user_count != 1 THEN
      RAISE e_user_not_found;
    ELSIF user_count > 1 THEN
      RAISE e_duplicate_users;    
    END IF;    
    
    -- check if users are already friends
    SELECT COUNT(*) INTO user_count
    FROM user_friends_tab ax
    WHERE ax.user_typ_1.uname = add_friend.uname1 AND ax.user_typ_2.uname = add_friend.uname2
    OR ax.user_typ_1.uname = add_friend.uname2 AND ax.user_typ_2.uname = add_friend.uname1;
    IF user_count >= 1 THEN
      RAISE e_user_already_friends;    
    END IF;
    
    INSERT INTO user_friends_tab
    SELECT REF(ax), REF(kx)
    FROM user_obj_tab ax, user_obj_tab kx
    WHERE ax.uname = add_friend.uname1 AND kx.uname = add_friend.uname2;    
    
    DBMS_OUTPUT.put_line('Users: ' || add_friend.uname1 || ' and ' || add_friend.uname2 || ' are now friends!');

    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('Username not found! Operation terminated.');
      WHEN e_duplicate_users THEN
        DBMS_OUTPUT.put_line('Duplicated users! Operation terminated.');
      WHEN e_user_already_friends THEN
        DBMS_OUTPUT.put_line('Users are already friends! Operation terminated.');
  END add_friend;
  
  PROCEDURE delete_friend (
    uname1 VARCHAR,
    uname2 VARCHAR
  ) IS
    e_user_not_friends EXCEPTION;
    user_count NUMBER;
  BEGIN
    -- check if users are friends
    SELECT COUNT(*) INTO user_count
    FROM user_friends_tab ax
    WHERE ax.user_typ_1.uname = delete_friend.uname1 AND ax.user_typ_2.uname = delete_friend.uname2
    OR ax.user_typ_1.uname = delete_friend.uname2 AND ax.user_typ_2.uname = delete_friend.uname1;
    IF user_count >= 1 THEN      
      DELETE FROM user_friends_tab uft 
      WHERE (uft.user_typ_1.uname = delete_friend.uname1 AND uft.user_typ_2.uname = delete_friend.uname2) 
      OR (uft.user_typ_1.uname = delete_friend.uname2 AND uft.user_typ_2.uname = delete_friend.uname1);      
      DBMS_OUTPUT.put_line('Users: ' || delete_friend.uname1 || ' and ' || delete_friend.uname2 || ' are no longer friends!');
    ELSE
      RAISE e_user_not_friends;      
    END IF;
    EXCEPTION
      WHEN e_user_not_friends THEN        
        DBMS_OUTPUT.put_line('Users: ' || uname1 || ' and ' || uname2 || ' are not friends! No changes were made.');      
  END delete_friend;
  
  PROCEDURE add_user_role (
    uname VARCHAR,
    role_name VARCHAR
  ) IS  
    e_user_not_found EXCEPTION;
    uuser user_typ;
    user_count NUMBER;
  BEGIN  
    SELECT VALUE(u) INTO uuser FROM user_obj_tab u WHERE u.uname=add_user_role.uname;
    IF uuser IS NULL THEN
      RAISE e_user_not_found;
    END IF;    
    uuser.add_user_role(add_user_role.role_name);
    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('Username not found! Operation terminated.');
  END add_user_role;
  
  PROCEDURE delete_user_role (
    uname VARCHAR,
    role_name VARCHAR
  ) IS  
    e_user_not_found EXCEPTION;
    uuser user_typ;
    user_count NUMBER;
  BEGIN  
    SELECT VALUE(u) INTO uuser FROM user_obj_tab u WHERE u.uname=delete_user_role.uname;
    IF uuser IS NULL THEN
      RAISE e_user_not_found;
    END IF;    
    uuser.delete_user_role(delete_user_role.role_name);
    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('Username not found! Operation terminated.');
  END delete_user_role;
  
  PROCEDURE write_user_data (
    uname VARCHAR
  ) AS  
    e_user_not_found EXCEPTION;
    obj_count NUMBER;
    uuser user_typ;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = write_user_data.uname;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    ELSE
      SELECT VALUE(u) INTO uuser FROM user_obj_tab u WHERE u.uname = write_user_data.uname;
      uuser.display_user_data;
    END IF;  
    EXCEPTION
      WHEN e_user_not_found THEN
        DBMS_OUTPUT.put_line('User:' || write_user_data.uname || ' not found! Operation terminated.');
  END write_user_data;
  
  PROCEDURE write_user_roles (
    uname VARCHAR
  ) AS
    e_user_not_found EXCEPTION;
    obj_count NUMBER;
    uuser user_typ;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = write_user_roles.uname;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    ELSE
      SELECT VALUE(u) INTO uuser FROM user_obj_tab u WHERE u.uname = write_user_roles.uname;
      uuser.write_user_roles;
    END IF;  
    EXCEPTION
      WHEN e_user_not_found THEN
        DBMS_OUTPUT.put_line('User:' || write_user_roles.uname || ' not found! Operation terminated.');
  END write_user_roles;
  
  PROCEDURE write_user_friends (
    uname VARCHAR
  ) AS  
    e_user_not_found EXCEPTION;    
    e_user_has_no_friends EXCEPTION;
    obj_count NUMBER;
    n NUMBER;
    uuser user_typ;
    has_any_record BOOLEAN;
    CURSOR friends_cursor IS SELECT DEREF(user_typ_2) FROM user_friends_tab uft WHERE uft.user_typ_1.uname=write_user_friends.uname;
  BEGIN 
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = write_user_friends.uname;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;   
    -- check if user has friends
    has_any_record := false;
    FOR friend IN friends_cursor
      LOOP
        has_any_record := true;
        EXIT;
      END LOOP;  
    IF has_any_record = false THEN
      RAISE e_user_has_no_friends;
    END IF;   
    -- let's write user friends
    n := 1;
    DBMS_OUTPUT.put_line('User: ' || write_user_friends.uname || ' friends:');
    OPEN friends_cursor;
      LOOP
        FETCH friends_cursor INTO uuser;
        EXIT WHEN friends_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(n || ': ' || uuser.uname);
        n := n + 1;
      END LOOP; 
    CLOSE friends_cursor;  
    EXCEPTION
      WHEN e_user_not_found THEN
        DBMS_OUTPUT.put_line('User: ' || write_user_friends.uname || ' not found! Operation terminated.');
      WHEN e_user_has_no_friends THEN
        DBMS_OUTPUT.PUT_LINE('User: ' || write_user_friends.uname || ' has no friends!');
  END write_user_friends;
  
  PROCEDURE write_user_participated_events (
    uname VARCHAR
  ) AS
    e_user_not_found EXCEPTION;    
    e_user_doesnt_participate EXCEPTION;
    obj_count NUMBER;
    n NUMBER;
    eevent event_typ;
    has_any_record BOOLEAN;
    CURSOR events_cursor IS SELECT DEREF(event_typ_1) FROM event_participants_tab ept WHERE ept.user_typ_1.uname=write_user_participated_events.uname;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = write_user_participated_events.uname;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;   
    -- check if user participates in any event
    has_any_record := false;
    FOR event IN events_cursor
      LOOP
        has_any_record := true;
        EXIT;
      END LOOP;  
    IF has_any_record = false THEN
      RAISE e_user_doesnt_participate;
    END IF;   
    -- let's write user friends
    n := 1;
    DBMS_OUTPUT.put_line('User: ' || write_user_participated_events.uname || ' participates in events:');
    OPEN events_cursor;
      LOOP
        FETCH events_cursor INTO eevent;
        EXIT WHEN events_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(n || ': ' || eevent.ename);
        n := n + 1;
      END LOOP; 
    CLOSE events_cursor;  
    EXCEPTION
      WHEN e_user_not_found THEN
        DBMS_OUTPUT.put_line('User: ' || write_user_participated_events.uname || ' not found! Operation terminated.');
      WHEN e_user_doesnt_participate THEN
        DBMS_OUTPUT.PUT_LINE('User: ' || write_user_participated_events.uname || ' doesn''t participate in any events.');
  END write_user_participated_events;
  
  PROCEDURE write_user_admin_events (
    uname VARCHAR
  ) AS
    e_user_not_found EXCEPTION;    
    e_user_doesnt_admin EXCEPTION;
    obj_count NUMBER;
    n NUMBER;
    eevent event_typ;
    has_any_record BOOLEAN;
    CURSOR events_cursor IS SELECT DEREF(event_typ_1) FROM event_administrators_tab ept WHERE ept.user_typ_1.uname=write_user_admin_events.uname;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = write_user_admin_events.uname;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;   
    -- check if user administers any event
    has_any_record := false;
    FOR event IN events_cursor
      LOOP
        has_any_record := true;
        EXIT;
      END LOOP;  
    IF has_any_record = false THEN
      RAISE e_user_doesnt_admin;
    END IF;   
    -- let's events administrated by user
    n := 1;
    DBMS_OUTPUT.put_line('User: ' || write_user_admin_events.uname || ' administers events:');
    OPEN events_cursor;
      LOOP
        FETCH events_cursor INTO eevent;
        EXIT WHEN events_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(n || ': ' || eevent.ename);
        n := n + 1;
      END LOOP; 
    CLOSE events_cursor;  
    EXCEPTION
      WHEN e_user_not_found THEN
        DBMS_OUTPUT.put_line('User: ' || write_user_admin_events.uname || ' not found! Operation terminated.');
      WHEN e_user_doesnt_admin THEN
        DBMS_OUTPUT.PUT_LINE('User: ' || write_user_admin_events.uname || ' doesn''t administer any events.');
  END write_user_admin_events;  
END USER_ACTIONS;
/
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE
PACKAGE BODY EVENT_ACTIONS AS

  PROCEDURE create_event (
  ename VARCHAR,
  beginning_date DATE,
  event_longitude NUMBER,
  event_latitude NUMBER,
  event_description VARCHAR
  ) AS
  event_name_taken EXCEPTION;
  event_longitude_bad_value EXCEPTION;
  event_latitude_bad_value EXCEPTION;
  event_count NUMBER;
  event_obj event_typ;
  BEGIN
    SELECT COUNT(*) INTO event_count FROM event_obj_tab e WHERE e.ename = create_event.ename;
    
    IF event_count > 0 THEN
      RAISE event_name_taken;
    ELSIF create_event.event_longitude < -180 OR create_event.event_longitude > 180 THEN
      RAISE event_longitude_bad_value;    
    ELSIF create_event.event_latitude < -90 OR create_event.event_latitude > 90 THEN
      RAISE event_latitude_bad_value;
    END IF;
    
    event_obj := event_typ(event_id_seq.NEXTVAL, create_event.ename, create_event.beginning_date, create_event.event_longitude, 
           create_event.event_latitude, create_event.event_description);
           
    INSERT INTO event_obj_tab VALUES(event_obj);
    DBMS_OUTPUT.PUT_LINE('Event: ' || create_event.ename || ' was successfully created!');
           
    EXCEPTION
      WHEN event_name_taken THEN
        DBMS_OUTPUT.PUT_LINE('Event name: ' || create_event.ename || ' is already taken. Please choose another one. No changes were made.');
      WHEN event_longitude_bad_value THEN
        DBMS_OUTPUT.PUT_LINE('Event longitude: ' || create_event.event_longitude || ' has wrong value. Please choose in range from -180 to 180.');
      WHEN event_latitude_bad_value THEN
        DBMS_OUTPUT.PUT_LINE('Event latitude: ' || create_event.event_latitude || ' has wrong value. Please choose in range from -90 to 90.');
  END create_event;

  PROCEDURE add_event_administrator (
    event_name VARCHAR,
    user_name VARCHAR
  ) AS
    e_user_not_found EXCEPTION;
    e_event_not_found EXCEPTION;
    e_user_already_admin EXCEPTION;
    obj_count NUMBER;
    uuser user_typ;
    eevent event_typ;
  BEGIN  
  
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = add_event_administrator.user_name;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;
    
    -- check if event is present
    SELECT COUNT(VALUE(e)) INTO obj_count FROM event_obj_tab e WHERE e.ename = add_event_administrator.event_name;        
    IF obj_count != 1 THEN
      RAISE e_event_not_found;
    END IF;
    
    -- check if user is already event's administrator.
    SELECT COUNT(*) INTO obj_count
    FROM event_administrators_tab eat
    WHERE eat.event_typ_1.ename = add_event_administrator.event_name AND eat.user_typ_1.uname = add_event_administrator.user_name;
    IF obj_count >= 1 THEN
      RAISE e_user_already_admin;    
    END IF;

    INSERT INTO event_administrators_tab
    SELECT REF(eot), REF(uot)
    FROM user_obj_tab uot, event_obj_tab eot
    WHERE uot.uname = add_event_administrator.user_name AND eot.ename = add_event_administrator.event_name;    

    DBMS_OUTPUT.put_line('User: ' || add_event_administrator.user_name || ' is now ADMIN of event: ' || add_event_administrator.event_name || '.');

    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('User: ' || add_event_administrator.user_name || ' not found! Operation terminated.');        
      WHEN e_event_not_found THEN
        DBMS_OUTPUT.put_line('Event: ' || add_event_administrator.event_name || ' not found! Operation terminated.');
      WHEN e_user_already_admin THEN
        DBMS_OUTPUT.put_line('User: ' || add_event_administrator.user_name || ' is already ADMIN of event: ' || add_event_administrator.event_name || '. No changes were made.');
  END add_event_administrator;

  PROCEDURE delete_event_administrator (
    event_name VARCHAR,
    user_name VARCHAR
  ) AS
    e_user_not_found EXCEPTION;
    e_event_not_found EXCEPTION;
    e_user_not_admin EXCEPTION;
    obj_count NUMBER;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = delete_event_administrator.user_name;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;
    
    -- check if event is present
    SELECT COUNT(VALUE(e)) INTO obj_count FROM event_obj_tab e WHERE e.ename = delete_event_administrator.event_name;        
    IF obj_count != 1 THEN
      RAISE e_event_not_found;
    END IF;
    
    -- check if user is already event's administrator.
    SELECT COUNT(*) INTO obj_count
    FROM event_administrators_tab eat
    WHERE eat.event_typ_1.ename = delete_event_administrator.event_name AND eat.user_typ_1.uname = delete_event_administrator.user_name;
    IF obj_count < 1 THEN
      RAISE e_user_not_admin;    
    END IF;

    DELETE FROM event_administrators_tab eat 
    WHERE eat.user_typ_1.uname = delete_event_administrator.user_name AND eat.event_typ_1.ename = delete_event_administrator.event_name;

    DBMS_OUTPUT.put_line('User: ' || delete_event_administrator.user_name || ' is NO MORE an ADMIN of event: ' || delete_event_administrator.event_name || '.');

    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('User: ' || delete_event_administrator.user_name || ' not found! Operation terminated.');        
      WHEN e_event_not_found THEN
        DBMS_OUTPUT.put_line('Event: ' || delete_event_administrator.event_name || ' not found! Operation terminated.');
      WHEN e_user_not_admin THEN
        DBMS_OUTPUT.put_line('User: ' || delete_event_administrator.user_name || ' is NOT an ADMIN of event: ' || delete_event_administrator.event_name || '. No changes were made.');
  END delete_event_administrator;

 PROCEDURE add_event_participant (
    event_name VARCHAR,
    user_name VARCHAR
  ) AS  
    e_user_not_found EXCEPTION;
    e_event_not_found EXCEPTION;
    e_user_already_participates EXCEPTION;
    obj_count NUMBER;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = add_event_participant.user_name;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;
    
    -- check if event is present
    SELECT COUNT(VALUE(e)) INTO obj_count FROM event_obj_tab e WHERE e.ename = add_event_participant.event_name;        
    IF obj_count != 1 THEN
      RAISE e_event_not_found;
    END IF;
    
    -- check if user is already event's participant.
    SELECT COUNT(*) INTO obj_count
    FROM event_participants_tab ept
    WHERE ept.event_typ_1.ename = add_event_participant.event_name AND ept.user_typ_1.uname = add_event_participant.user_name;
    IF obj_count >= 1 THEN
      RAISE e_user_already_participates;    
    END IF;
    
    INSERT INTO event_participants_tab
    SELECT REF(eot), REF(uot)
    FROM user_obj_tab uot, event_obj_tab eot
    WHERE uot.uname = add_event_participant.user_name AND eot.ename = add_event_participant.event_name;    

    DBMS_OUTPUT.put_line('User: ' || add_event_participant.user_name || ' is now a PARTICIPANT of event: ' || add_event_participant.event_name || '.');

    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('User: ' || add_event_participant.user_name || ' not found! Operation terminated.');        
      WHEN e_event_not_found THEN
        DBMS_OUTPUT.put_line('Event: ' || add_event_participant.event_name || ' not found! Operation terminated.');
      WHEN e_user_already_participates THEN
        DBMS_OUTPUT.put_line('User: ' || add_event_participant.user_name || ' is ALREADY a PARTICIPANT of event: ' || add_event_participant.event_name || '. No changes were made.');  
  END add_event_participant;

  PROCEDURE delete_event_participant (
    event_name VARCHAR,
    user_name VARCHAR
  ) AS  
    e_user_not_found EXCEPTION;
    e_event_not_found EXCEPTION;
    e_user_doesnt_participate EXCEPTION;
    obj_count NUMBER;
  BEGIN
    -- check if user is present
    SELECT COUNT(VALUE(u)) INTO obj_count FROM user_obj_tab u WHERE u.uname = delete_event_participant.user_name;        
    IF obj_count != 1 THEN
      RAISE e_user_not_found;
    END IF;
    
    -- check if event is present
    SELECT COUNT(VALUE(e)) INTO obj_count FROM event_obj_tab e WHERE e.ename = delete_event_participant.event_name;        
    IF obj_count != 1 THEN
      RAISE e_event_not_found;
    END IF;
    
    -- check if user is already event's participant.
    SELECT COUNT(*) INTO obj_count
    FROM event_participants_tab ept
    WHERE ept.event_typ_1.ename = delete_event_participant.event_name AND ept.user_typ_1.uname = delete_event_participant.user_name;
    IF obj_count < 1 THEN
      RAISE e_user_doesnt_participate;    
    END IF;
    
    DELETE FROM event_participants_tab ept
    WHERE ept.user_typ_1.uname = delete_event_participant.user_name AND ept.event_typ_1.ename = delete_event_participant.event_name;    

    DBMS_OUTPUT.put_line('User: ' || delete_event_participant.user_name || ' is NO MORE a PARTICIPANT of event: ' || delete_event_participant.event_name || '.');

    EXCEPTION
      WHEN e_user_not_found THEN        
        DBMS_OUTPUT.put_line('User: ' || delete_event_participant.user_name || ' not found! Operation terminated.');        
      WHEN e_event_not_found THEN
        DBMS_OUTPUT.put_line('Event: ' || delete_event_participant.event_name || ' not found! Operation terminated.');
      WHEN e_user_doesnt_participate THEN
        DBMS_OUTPUT.put_line('User: ' || delete_event_participant.user_name || ' is NOT a PARTICIPANT of event: ' || delete_event_participant.event_name || '. No changes were made.');  
  END delete_event_participant;

  PROCEDURE write_event_administrators (
    event_name VARCHAR
  ) AS
    CURSOR event_cursor IS SELECT DEREF(user_typ_1) FROM event_administrators_tab ept WHERE ept.event_typ_1.ename=write_event_administrators.event_name;
    uuser user_typ;
    n NUMBER;
  BEGIN  
    DBMS_OUTPUT.PUT_LINE(write_event_administrators.event_name || ' administrators are: ');
    n := 1;
    OPEN event_cursor;
      LOOP
        FETCH event_cursor INTO uuser;
        EXIT WHEN event_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(n || ': ' || uuser.uname);
      END LOOP;
  END write_event_administrators;

  PROCEDURE write_event_participants (
    event_name VARCHAR
  ) AS
    CURSOR event_cursor IS SELECT DEREF(user_typ_1) FROM event_participants_tab ept WHERE ept.event_typ_1.ename=write_event_participants.event_name;
    uuser user_typ;
    n NUMBER;
  BEGIN
    DBMS_OUTPUT.PUT_LINE(write_event_participants.event_name || ' participants are: ');
    n := 1;
    OPEN event_cursor;
      LOOP
        FETCH event_cursor INTO uuser;
        EXIT WHEN event_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(n || ': ' || uuser.uname);
      END LOOP;
  END write_event_participants;
  
  PROCEDURE write_event_details (
    event_name VARCHAR
  ) AS  
    e_event_not_found EXCEPTION;
    obj_count NUMBER;
    eevent event_typ;
  BEGIN
    -- check if event is present
    SELECT COUNT(VALUE(e)) INTO obj_count FROM event_obj_tab e WHERE e.ename = write_event_details.event_name;        
    IF obj_count != 1 THEN
      RAISE e_event_not_found;
    END IF;
    
    SELECT VALUE(e) INTO eevent FROM event_obj_tab e WHERE e.ename = write_event_details.event_name;
    eevent.display_event_data;
    
    EXCEPTION     
      WHEN e_event_not_found THEN
        DBMS_OUTPUT.put_line('Event: ' || write_event_details.event_name || ' not found! Operation terminated.');
  END write_event_details;
END EVENT_ACTIONS;