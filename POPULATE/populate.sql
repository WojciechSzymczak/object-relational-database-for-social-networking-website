-- Projekt z przedmiotu: Obiektowe Bazy Danych.
-- Autor: Wojciech Szymczak.

SET SERVEROUTPUT ON;
DECLARE
  lon NUMBER(9,6);
  lat NUMBER(8,6);
BEGIN
  lon := 178.988;
  lat := 87.78555;
  USER_ACTIONS.register_user('maciek@email.com', 'maciek', 'haslo123');
  USER_ACTIONS.register_user('jasiek@email.com', 'jasiek', 'haslo123');
  USER_ACTIONS.register_user('mariusz@email.com', 'mariusz', 'haslo123');
  USER_ACTIONS.update_user_details('maciek', 'maciej', 'nowak', '1933-04-12');  
  USER_ACTIONS.add_friend('jasiek', 'mariusz'); 
  USER_ACTIONS.add_friend('jasiek', 'maciek');
  USER_ACTIONS.add_user_role('jasiek', 'USER1');
  USER_ACTIONS.add_user_role('jasiek', 'USER2');
  USER_ACTIONS.add_user_role('jasiek', 'USER3');  
  USER_ACTIONS.delete_user_role('jasiek', 'USER3');
  USER_ACTIONS.delete_friend('jasiek', 'maciek');  
  EVENT_ACTIONS.create_event('Matthew''s Birthday.', '2019-03-04', lon, lat, 'Matthew will be drunk.');
  EVENT_ACTIONS.create_event('John''s Birthday.', '2019-12-01', lon, lat, 'John will be drunk.');
  EVENT_ACTIONS.add_event_administrator('John''s Birthday.', 'jasiek');
  EVENT_ACTIONS.add_event_administrator('John''s Birthday.', 'maciek');
  EVENT_ACTIONS.add_event_administrator('John''s Birthday.', 'mariusz');
  EVENT_ACTIONS.delete_event_administrator('John''s Birthday.', 'maciek');  
  EVENT_ACTIONS.add_event_participant('Matthew''s Birthday.', 'jasiek');  
  EVENT_ACTIONS.add_event_participant('John''s Birthday.', 'maciek');  
  EVENT_ACTIONS.add_event_participant('John''s Birthday.', 'jasiek');
  EVENT_ACTIONS.delete_event_participant('John''s Birthday.', 'jasiek');
  EVENT_ACTIONS.add_event_participant('John''s Birthday.', 'jasiek');
  
  EVENT_ACTIONS.write_event_details('John''s Birthday.');
  EVENT_ACTIONS.write_event_administrators('John''s Birthday.');
  EVENT_ACTIONS.write_event_participants('John''s Birthday.');  
  USER_ACTIONS.write_user_data('jasiek');
  USER_ACTIONS.write_user_roles('jasiek');
  USER_ACTIONS.write_user_friends('jasiek');
  USER_ACTIONS.write_user_friends('mariusz');
  USER_ACTIONS.write_user_participated_events('jasiek');
  USER_ACTIONS.write_user_participated_events('mariusz');
  USER_ACTIONS.write_user_admin_events('jasiek');
  USER_ACTIONS.write_user_admin_events('mariusz');
  USER_ACTIONS.write_user_admin_events('maciek');
END;
/
--SELECT * FROM user_obj_tab;
--SELECT * FROM user_friends_tab;
--SELECT * FROM event_obj_tab;
--SELECT * FROM event_administrators_tab;
--SELECT * FROM event_participants_tab;