-- Projekt z przedmiotu: Obiektowe Bazy Danych.
-- Autor: Wojciech Szymczak.
DROP TABLE user_friends_tab;
DROP TABLE user_obj_tab;
DROP TABLE event_obj_tab;
DROP TABLE event_participants_tab;
DROP TABLE event_administrators_tab;

DROP SEQUENCE user_id_seq;
DROP SEQUENCE user_details_id_seq;
DROP SEQUENCE event_id_seq;

DROP TYPE user_typ;
DROP TYPE user_details_typ;
DROP TYPE role_tab;
DROP TYPE role_typ;
DROP TYPE event_typ;

DROP PACKAGE user_actions;
DROP PACKAGE event_actions;